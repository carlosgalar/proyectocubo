﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.Translate ( Input.GetAxis ( "Horizontal" ) * Vector3.forward * 10f  * Time.deltaTime );
		
		transform.Translate ( Input.GetAxis ( "Vertical" ) * Vector3.right * 10f  * Time.deltaTime );

	}
}
